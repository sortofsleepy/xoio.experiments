var rsvp = require('rsvp');
class DataFetcher {
    constructor(){
        var url = "https://bitbucket.org/api/1.0/repositories/xoio/xoio.bitbucket.org/src/master/";
        var req = new XMLHttpRequest();
        req.open("GET",url);
        req.send(null);

        return new rsvp.Promise(function(resolve,reject){
           req.onreadystatechange = function(){
               if(req.readyState === 4){

                   if(req.status === 200){
                       const data = JSON.parse(req.responseText);
                       resolve(data);
                   }else{
                       reject([],req.responseText);
                   }

               }
           }

        })
    }
}

export default DataFetcher;