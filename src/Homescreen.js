const raf = require('raf');
const glslify = require('glslify');
const GrainPass = require('./GrainPass');


WAGNER.vertexShadersPath = "/js/wagner/vertex-shaders";
WAGNER.fragmentShadersPath = "/js/wagner/fragment-shaders";

var renderer = new THREE.WebGLRenderer({
    alpha:1
});
renderer.setClearColor(0xdddddd);
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75.0,window.innerWidth / window.innerHeight, 1.0, 10000);
camera.position.z = 70;
renderer.domElement.id = "home_canvas";
renderer.setSize(window.innerWidth,window.innerHeight);
document.querySelector("#home_canvas").appendChild(renderer.domElement);

var composer = new WAGNER.Composer( renderer, { useRGBA: false } );
composer.setSize( renderer.domElement.width, renderer.domElement.height );

var grainPass = new WAGNER.GrainPass();


window.addEventListener('resize',function(){
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
    composer.setSize( renderer.domElement.width, renderer.domElement.height );

});

/////// build scene //////////

scene.fog = new THREE.FogExp2( 0xefd1b5, 0.0025 );


var iso = new THREE.PlaneBufferGeometry(400,400);

var mat = new THREE.ShaderMaterial({
    vertexShader:glslify('./shaders/background-v.glsl'),
    fragmentShader:glslify('./shaders/background-f.glsl'),
    uniforms:{
        time:{
            type:'f',
            value:0.0
        },

        iResolution:{
            type:'v2',
            value:new THREE.Vector2(window.innerWidth,window.innerHeight)
        }
    }
});

var isoMat = new THREE.Mesh(iso,mat);
scene.add(isoMat);


var counter = 0;
raf(function tick(){

    //renderer.render(scene,camera);
   // isoMat.rotation.x += 0.001;
    mat.uniforms.time.value = performance.now() * 0.001;
    composer.reset();
    composer.render(scene,camera);
    composer.pass(grainPass);
    composer.toScreen();

    raf(tick);
});