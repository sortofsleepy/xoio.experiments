var glslify = require('glslify');

WAGNER.GrainPass = function() {

    WAGNER.Pass.call( this );
    this.shader = new THREE.ShaderMaterial({
        vertexShader:glslify('./shaders/grain-vert.glsl'),
        fragmentShader:glslify('./shaders/grain-frag.glsl'),
        uniforms: {
            "tInput":{
                type:'t',
                value:null
            },
            "resolution":{
                type:'v2',
                value:new THREE.Vector2()
            },
            "tDiffuse":   { type: "t", value: null },
            "time":       { type: "f", value: 0.0 },
            "nIntensity": { type: "f", value: 0.2 },
            "sIntensity": { type: "f", value: 0.00005 },
            "sCount":     { type: "f", value: 4096 },
            "grayscale":  { type: "i", value: 1 }

        }
    })

    this.params.resolution = new THREE.Vector2(window.innerWidth,window.innerHeight);


};

WAGNER.GrainPass.prototype = Object.create( WAGNER.Pass.prototype );

WAGNER.GrainPass.prototype.isLoaded = function() {


    return WAGNER.Pass.prototype.isLoaded.call( this );

};

WAGNER.GrainPass.prototype.run = function( c ) {

    c.pass(this.shader);
};

module.exports = WAGNER.GrainPass;