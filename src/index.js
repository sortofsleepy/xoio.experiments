const ReactDOM = require('react-dom');
const React = require('react');
const HomeScreen = require('./Homescreen');

window.React = React;
var AppComponent = require('./components/AppComponent');


var app = document.querySelector("#SITE");

Typekit.load({
    loading:function(){
        //todo add loading animation
    },

    active:function(){

       if(device.mobile()){
           document.querySelector("#mobile-notice").classList.remove('hide');
       }else{
           //render project list
           ReactDOM.render(React.createElement(AppComponent,null), app);
       }
    },
    async:true
});

