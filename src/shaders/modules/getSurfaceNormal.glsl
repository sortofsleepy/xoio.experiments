

// Calculates the normal direction so that we ca
vec3 getNormalOfSurface( in vec3 positionOfHit ){

	vec3 tinyChangeX = vec3( 0.001, 0.0, 0.0 );
    vec3 tinyChangeY = vec3( 0.0 , 0.001 , 0.0 );
    vec3 tinyChangeZ = vec3( 0.0 , 0.0 , 0.001 );

   	float upTinyChangeInX   = map( positionOfHit + tinyChangeX ).x;
    float downTinyChangeInX = map( positionOfHit - tinyChangeX ).x;

    float tinyChangeInX = upTinyChangeInX - downTinyChangeInX;


    float upTinyChangeInY   = map( positionOfHit + tinyChangeY ).x;
    float downTinyChangeInY = map( positionOfHit - tinyChangeY ).x;

    float tinyChangeInY = upTinyChangeInY - downTinyChangeInY;


    float upTinyChangeInZ   = map( positionOfHit + tinyChangeZ ).x;
    float downTinyChangeInZ = map( positionOfHit - tinyChangeZ ).x;

    float tinyChangeInZ = upTinyChangeInZ - downTinyChangeInZ;


	vec3 normal = vec3(
         			tinyChangeInX,
        			tinyChangeInY,
        			tinyChangeInZ
    	 		  );

	return normalize(normal);
}

#pragma glslify: export(getNormalOfSurface)