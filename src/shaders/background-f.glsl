
// define the maximum number of times we raymarch.
#define MAX_STEPS 100

// defines how close we can get before we've hit the scene
#define HOW_CLOSE 0.001

// defines how far the ray can go
#define FURTHEST_OUR_RAY_CAN_REACH 30.0
varying vec2 vUv;
uniform vec2 iResolution;
uniform float time;
#pragma glslify: calculateEyeRayTransformationMatrix = require(./modules/setCamera.glsl)
#pragma glslify: snoise = require(./modules/noise3d.glsl)





vec2 opU( vec2 d1, vec2 d2 )
{
	return (d1.x<d2.x) ? d1 : d2;
}


float sdPlane( vec3 p )
{
	return p.y;
}
vec2 map(vec3 currentPos){

    vec2 result = vec2(0.);
    float plane = sdPlane(vec3(currentPos.x,currentPos.y += 0.4,currentPos.z));

    //shift sphere slightly up
    currentPos.y -= 0.5;

    //start things off by making a sphere
  	result = vec2( plane,plane );

  	//add a plane
  	result = opU(result,vec2(plane,0.0));
	//result = opU(result,sdfBalloon(currentPos));
    return result;
}


// Calculates the normal direction so that we ca
vec3 getNormalOfSurface( in vec3 positionOfHit ){

	vec3 tinyChangeX = vec3( 0.001, 0.0, 0.0 );
    vec3 tinyChangeY = vec3( 0.0 , 0.001 , 0.0 );
    vec3 tinyChangeZ = vec3( 0.0 , 0.0 , 0.001 );

   	float upTinyChangeInX   = map( positionOfHit + tinyChangeX ).x;
    float downTinyChangeInX = map( positionOfHit - tinyChangeX ).x;

    float tinyChangeInX = upTinyChangeInX - downTinyChangeInX;


    float upTinyChangeInY   = map( positionOfHit + tinyChangeY ).x;
    float downTinyChangeInY = map( positionOfHit - tinyChangeY ).x;

    float tinyChangeInY = upTinyChangeInY - downTinyChangeInY;


    float upTinyChangeInZ   = map( positionOfHit + tinyChangeZ ).x;
    float downTinyChangeInZ = map( positionOfHit - tinyChangeZ ).x;

    float tinyChangeInZ = upTinyChangeInZ - downTinyChangeInZ;


	vec3 normal = vec3(
         			tinyChangeInX,
        			tinyChangeInY,
        			tinyChangeInZ
    	 		  );

	return normalize(normal);
}

vec3 calcRay(in vec3 ro, in vec3 rd ){
    vec3 result = vec3(0.);

    float min = HOW_CLOSE;
    float max = FURTHEST_OUR_RAY_CAN_REACH;

    float finalDistanceTraveledByRay = min;
    float finalID = -1.0;

    for( int i = 0; i < MAX_STEPS; i++ ){

        //march something!
        vec2 res = map( ro + rd * finalDistanceTraveledByRay );


        //if we're less than 0, thats too far, so break
        if(res.x < min || finalDistanceTraveledByRay > max){
        	break;
        }

        finalDistanceTraveledByRay += res.x;
        finalID = res.y;

    }


    if(finalDistanceTraveledByRay > max){
     	finalID = -1.0;
    }

    result.x = finalDistanceTraveledByRay;
    result.y = finalID;
    return result;
}

float fogFactorExp2_529295689(const float dist,const float density) {
  const float LOG2 = -1.442695;
  float d = density * dist;
  return 1.0 - clamp(exp2(d * d * LOG2), 0.0, 1.0);
}

highp float random_2281831123(vec2 co)
{
    highp float a = 12.9898;
    highp float b = 78.233;
    highp float c = 43758.5453;
    highp float dt= dot(co.xy ,vec2(a,b));
    highp float sn= mod(dt,3.14);
    return fract(sin(sn) * c);
}

///////// GRID FLOOR ////////
// Source: http://www.iquilezles.org/www/articles/palettes/palettes.htm
// Source: @hughsk https://www.shadertoy.com/view/4d3GW7
vec3 palette( in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d ) {
    return a + b*cos( 6.28318*(c * t * d) );
}
float intersectPlane(vec3 ro, vec3 rd, vec3 nor, float dist) {
  float denom = dot(rd, nor);
  float t = -(dot(ro, nor) * dist + dist) / denom;
  t += t;
  return t;
}
vec3 bg(vec3 ro, vec3 rd) {
  vec3 col = 0.1 + (
    palette(clamp((random_2281831123(rd.xz + time * 0.1) * 0.5 + 0.5) * 0.05 - rd.y * 0.5 + 0.35, -1.0, 1.0)
      , vec3(0.5, 0.45, 0.55)
      , vec3(0.5, 0.5, 0.5)
      , vec3(1.05, 1.0, 1.0)
      , vec3(0.575, 0.2, 0.19)
    )
  );

  float t = intersectPlane(ro, rd, vec3(0, 1, 0), 3.);

  if (t > 0.0) {
    vec3 p = ro + rd * t;
    float g = (1.0 - pow(abs(sin(p.x) * cos(p.z)), 0.25));

    col += (1.0 - fogFactorExp2_529295689(t, 0.04)) * g * vec3(5, 4, 2) * cos(time)* 10.0 + snoise(col);

  }

  return col;
}

//handle coloring of objects
vec3 colorTheWorld( in vec2 rayHitInfo , in vec3 eyePosition , in vec3 rayDirection ){
	vec3 color = vec3(0.);
	vec3 positionOfHit = eyePosition + rayHitInfo.x * rayDirection;
	vec3 normalOfSurface = getNormalOfSurface( positionOfHit );
  vec3 ref = refract(rayDirection, normalOfSurface, 1.09);

         color = bg(eyePosition + ref * 0.1, ref) * 0.5;



    return color;
}
// handle rendering of actual objects
vec3 render(in vec3 ro, in vec3 rd){

    //our output result
 	vec3 col = vec3(0.8, 0.9, 1.0);

    //calculate the ray interesections
    vec3 res = calcRay(ro,rd);

    float t = res.x;

    //get reference to id
	float id = res.y;

    //render all the items
     if( id>-0.5 )
    {
        vec3 pos = ro + t*rd;
        vec3 nor = getNormalOfSurface( pos );
        vec3 ref = reflect( rd, nor );
        vec3 color = bg(ro, rd);


        col = res;
    }

    return col;

}


void main(){
	vec2 currentPixel = ( -iResolution.xy + 2.0 * gl_FragCoord.xy ) / iResolution.y;

// We use the eye position to tell use where the viewer is
    vec3 eyePosition = vec3( -0.5+3.5*cos(0.1*time + 6.0), 1.0 + 2.0, 0.5 + 3.5*sin(0.1*time + 6.0) );

    // This is the point the view is looking at.
    // The window will be placed between the eye, and the
    // position the eye is looking at!
    vec3 pointWeAreLookingAt =  vec3( -0.5, -0.4, 0.5 );


    //build the camera
    mat3 cam = calculateEyeRayTransformationMatrix(eyePosition,pointWeAreLookingAt,0.0);

   	vec3 rayComingOutOfEyeDirection = normalize( cam * vec3( currentPixel.xy , 2. ) );
  	vec3 rayHitInfo = render( eyePosition , rayComingOutOfEyeDirection );

   	vec3 t = colorTheWorld(rayHitInfo.xy,eyePosition,rayComingOutOfEyeDirection);

   	//gl_FragColor = vec4(1.0,1.0,0.0,1.0);
   	gl_FragColor = vec4(t,1.0);
}