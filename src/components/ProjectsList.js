const React = window.React;
import DataFetcher from "../utils/DataFetcher"


var ProjectsList = React.createClass({
    getInitialState:function(){
        return {
            defaultViewerTag:"#VIEWER",
            viewer:null,
            experiments:[]
        }
    },


    handleProjectClick:function(e){
        this.props.loadProject(e.target.innerHTML.toLowerCase());
    },

    showLoadedProjects(){
        const projects = document.getElementsByClassName("projects__not__loaded")[0];
        projects.classList.remove("projects__not__loaded");
    },

    componentDidMount:function(){
        var self = this;
        var container = this.refs['project-container'];
        var leftColumn = document.querySelector("#left-column");
        var projects = document.getElementsByClassName("projects__not__loaded")[0];

        TweenMax.to(leftColumn,0.5,{
            opacity:1
        });

        // setup data sources
        const req = new DataFetcher();
        req.then(function(data){

            var experiments = [];
            var counter = 0;
            for(var i in data.directories){
                experiments.push({
                    id:counter,
                    name:data.directories[i]
                });

                counter++;
            }
            projects.classList.remove("projects__not__loaded");


            self.setState({experiments:experiments});

            // build onto the project lists so we can do maybe something interesting with them
            self.state.experiments.forEach((experiment) => {
                experiment.acceleration = Vector();
                experiment.velocity = Vector();
                experiment.position = Vector();
            });



        },function(err,res){
            console.log(err + "- repsonse was : ",res);
        });

        //set reference to default viewer
        this.state.viewer = document.querySelector(this.state.defaultViewerTag);



    },
    render:function(){

        var self = this;
        return (
            <div id="projects" ref="projects-container">
                <ul className="projects__not__loaded">
                    {
                        this.state.experiments.map(function(experiment){
                            return <li className="project-item" onClick={self.handleProjectClick} key={experiment.id}>{experiment.name}</li>
                        })
                    }
                </ul>

                <div id="info">
                    <p> New site in the works for the new year. In the meantime, enjoy some experiments.</p>
                    <br/>
                    <p>Source for experiments can be found on <a href="http://bitbucket.org/xoio/xoio.bitbucket.com" target="_blank">Bitbucket</a></p>
                    <br/>
                    <p> In the meantime, you can get the run-down of all the professional projects I've been associated with
                        on <a href="http://linkedin.com/in/sortofsleepy" target="_blank">LinkedIn</a></p>
                    <br/>
                    <p> Feel free to reach out at <a href="http://twitter.com/sortofsleepy" target="_blank">@sortofsleepy</a></p>
                </div>

            </div>
        )
    }
});


module.exports = ProjectsList;