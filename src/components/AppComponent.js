const React = window.React;
import ProjectList from "./ProjectsList"
import ViewerComponent from "./ViewerComponent"

const homescreen = document.querySelector("#home_canvas");
const loadIndicator = document.querySelector("#load_notice");
const viewer = document.querySelector("#viewer-frame");
const bitbucketUrl = "xoio.bitbucket.com/"
const projectPublicPath = "/public/index.html";

var AppComponent = React.createClass({

    getInitialState:function(){
        return {
            currentExperiment:null,
            shaderToyEmbedUrl:"https://www.shadertoy.com/embed/"
        }
    },
    notifyStateChange:function(){},
    loadProject:function(project_name){
        //only change projects if we aren't already viewing that project
        if(this.state.currentExperiment !== project_name){
            this.setState({
                currentExperiment:project_name
            });
        }
    },

    render:function(){
        return (
            <div className="wrap">
                <div id="left-column">
                    <ProjectList loadProject={this.loadProject} stateChanged={this.notifyStateChange}/>
                </div>
                <div id="right-column">
                    <ViewerComponent currentProject={this.state.currentExperiment}/>
                </div>
            </div>
        )
    }
});


module.exports = AppComponent;