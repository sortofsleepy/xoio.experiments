const React = window.React;


const homescreen = document.querySelector("#home_canvas");
const loadIndicator = document.querySelector("#load_notice");
const viewer = document.querySelector("#viewer-frame");
const bitbucketUrl = "http://xoio.bitbucket.com/"
const projectPublicPath = "/public/index.html";

var ViewerComponent = React.createClass({
    getInitialState:function(){
        return {
            currentProject:this.props.currentProject,
            hasLoaded:false,
            frame:null
        }
    },
    shouldComponentUpdate(nextProps,nextState){
        var self = this;
        var viewParent = self.refs["view-parent"];
        var firstChild = self.refs["viewer-parent"].children[0];
        var viewableChild = self.refs["viewer-parent"].children[1];
        loadIndicator.classList.add("show");



        this.refs["viewer-parent"].children[0].onload = function(){

            //TODO not sure why we can't use the refs above but oh well.
            var viewParent = self.refs["view-parent"];
            var firstChild = self.refs["viewer-parent"].children[0];
            var viewableChild = self.refs["viewer-parent"].children[1];
            if(!self.state.hasLoaded){
                TweenMax.to(homescreen,1.0,{
                    left:window.innerWidth,
                    ease:Power4.easeInOut
                });

                self.state.hasLoaded = true;

            }else{
                TweenMax.to(viewableChild,1.0,{
                    left:window.innerWidth,
                    ease:Power4.easeInOut,
                    onComplete:function(){
                        firstChild.style.zIndex = 99;
                        self.refs["viewer-parent"].removeChild(viewableChild);
                    }
                })

            }
            //prepare new iframe for next click
            var frame = document.createElement("iframe");
            frame.classList.add('viewer-frame');
           // frame.style.zIndex = 90;

            self.refs["viewer-parent"].insertBefore(frame,self.refs["viewer-parent"].children[0]);

            loadIndicator.classList.remove("show");

        };

        firstChild.src =`http://xoio.bitbucket.org/${nextProps.currentProject}/public/index.html`;
        //firstChild.src =`http://xoio.bitbucket.org/particle_sphere/public/index.html`;

        return true;
    },
    componentDidMount:function(){
        var self = this;
        //prepare new iframe for next click
        var frame = document.createElement("iframe");
        frame.classList.add('viewer-frame');

        this.refs['viewer-parent'].appendChild(frame);
    },
    render:function(){
        return (
           <div ref="viewer-parent" id="VIEWER">

           </div>
        )
    }
});


module.exports = ViewerComponent;